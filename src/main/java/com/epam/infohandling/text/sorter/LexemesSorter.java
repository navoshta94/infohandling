package com.epam.infohandling.text.sorter;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Value;

import java.util.Arrays;
import java.util.List;

/**
 * Created by e.navosha on 26.11.18.
 */
public class LexemesSorter {

    public Component sort(Component text, char symbol) {
        List<Component> paragraphsForSort = text.getChildren();
        Component sortedText = new Composite();
        for (Component paragraph : paragraphsForSort) {
            List<Component> sentences = paragraph.getChildren();
            Component sortedParagraph = new Composite();
            sortedText.addComponent(sortedParagraph);
            for (Component sentence : sentences) {
                sortedParagraph.addComponent(sortWordsBySymbolInput(sentence, symbol));
            }
        }

        return sortedText;
    }

    private Component sortWordsBySymbolInput(Component sentence, char symbol) {
        Component sortedSentence = new Composite();
        List<Component> lexemes = sentence.getChildren();
        lexemes.sort((a, b) -> {
            Value firstLexeme = (Value) a;
            Value secondLexeme = (Value) b;
            String firstValue = firstLexeme.getValue();
            String secondValue = secondLexeme.getValue();
            int firstValueSymbolInput = amountSymbolInput(firstValue, symbol);
            int secondValueSymbolInput = amountSymbolInput(secondValue, symbol);
            if (firstValueSymbolInput == secondValueSymbolInput) {
                String[] strings = {firstValue, secondValue};
                Arrays.sort(strings);
                if (strings[0] == firstValue) {
                    return -1;
                } else {
                    return 1;
                }
            }
            return secondValueSymbolInput - firstValueSymbolInput;
        });
        lexemes.forEach(sortedSentence::addComponent);
        return sortedSentence;
    }

    private int amountSymbolInput(String lexeme, char symbol) {
        char[] symbols = lexeme.toCharArray();
        int counter = 0;
        for (int i = 0; i < symbols.length; i++) {
            if (symbols[i] == symbol) {
                counter++;
            }
        }
        return counter;
    }
}
