package com.epam.infohandling;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;

import java.util.Arrays;
import java.util.List;

/**
 * Created by e.navosha on 23.11.18.
 */
public class TestObjects {
    public static Component testText = initialize();

    private static Component initialize() {
        Component text = new Composite();

        Component firstParagraph = new Composite();
        Component secondParagraph = new Composite();
        Component thirdParagraph = new Composite();
        text.addComponent(firstParagraph);
        text.addComponent(secondParagraph);
        text.addComponent(thirdParagraph);
        //1-1
        Component firstSentenceFirstParagraph = new Composite();
        firstSentenceFirstParagraph.addComponent(Lexeme.word("My"));
        firstSentenceFirstParagraph.addComponent(Lexeme.word("name"));
        firstSentenceFirstParagraph.addComponent(Lexeme.word("is"));
        firstSentenceFirstParagraph.addComponent(Lexeme.word("Eugene"));
        firstParagraph.addComponent(firstSentenceFirstParagraph);
        //1-2
        Component secondSentenceFirstParagraph = new Composite();
        secondSentenceFirstParagraph.addComponent(Lexeme.word("I"));
        secondSentenceFirstParagraph.addComponent(Lexeme.word("am"));
        secondSentenceFirstParagraph.addComponent(Lexeme.expression("32*2/4+8"));
        secondSentenceFirstParagraph.addComponent(Lexeme.word("years"));
        secondSentenceFirstParagraph.addComponent(Lexeme.word("old"));
        firstParagraph.addComponent(secondSentenceFirstParagraph);
        //2-1
        Component firstSentenceSecondParagraph = new Composite();
        firstSentenceSecondParagraph.addComponent(Lexeme.word("I"));
        firstSentenceSecondParagraph.addComponent(Lexeme.word("have"));
        firstSentenceSecondParagraph.addComponent(Lexeme.expression("3+2/2*4-1"));
        firstSentenceSecondParagraph.addComponent(Lexeme.word("apples"));
        secondParagraph.addComponent(firstSentenceSecondParagraph);
        //3-1
        Component firstSentenceThirdParagraph = new Composite();
        firstSentenceThirdParagraph.addComponent(Lexeme.word("This"));
        firstSentenceThirdParagraph.addComponent(Lexeme.word("is"));
        firstSentenceThirdParagraph.addComponent(Lexeme.word("test"));
        firstSentenceThirdParagraph.addComponent(Lexeme.word("text!"));
        thirdParagraph.addComponent(firstSentenceThirdParagraph);
        //3-2
        Component secondSentenceThirdParagraph = new Composite();
        secondSentenceThirdParagraph.addComponent(Lexeme.word("Hello?"));
        thirdParagraph.addComponent(secondSentenceThirdParagraph);
        //3-3
        Component thirdSentenceThirdParagraph = new Composite();
        thirdSentenceThirdParagraph.addComponent(Lexeme.word("Bye..."));
        thirdParagraph.addComponent(thirdSentenceThirdParagraph);

        return text;
    }
}
