package com.epam.infohandling.expression.nonterminal;

import com.epam.infohandling.expression.AbstractMathExpression;
import com.epam.infohandling.expression.Context;

/**
 * Created by e.navosha on 21.11.18.
 */
public class NonterminalExpression extends AbstractMathExpression {
    private double number;

    public NonterminalExpression(double number) {
        this.number = number;
    }
    @Override
    public void interpret(Context context) {
        context.pushValue(number);
    }
}
