package com.epam.infohandling.comparator;

import com.epam.infohandling.entity.Component;

import java.util.Comparator;
import java.util.List;

/**
 * Created by e.navosha on 26.11.18.
 */
public class ParagraphComparator implements Comparator<Component>{


    @Override
    public int compare(Component firstParagraph, Component secondParagraph) {
        List<Component> firstParagraphSentences = firstParagraph.getChildren();
        List<Component> secondParagraphSentences = secondParagraph.getChildren();
        return firstParagraphSentences.size() - secondParagraphSentences.size();
    }
}
