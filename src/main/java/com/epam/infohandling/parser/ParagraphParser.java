package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser  extends Parser{
    private static final String PATTERN_REGEX = "(\\w+\\s*([^\\.\\!\\?])*\\s*)+" +
            "(\\.\\.\\.|\\.|\\!|\\?)";
    private static final Pattern pattern = Pattern.compile(PATTERN_REGEX);

    private Parser successor;

    public ParagraphParser() {

    }

    public ParagraphParser(Parser successor) {
        this.successor = successor;
    }

    @Override
    public Component parse(String paragraph) {
        List<String> sentences = acquireSentences(paragraph);
        Component component = new Composite();
        sentences.forEach(o -> component.addComponent(successor.parse(o)));
        return component;
    }

    private List<String> acquireSentences(String paragraph) {
        List<String> sentences = new ArrayList<>();
        Matcher matcher = pattern.matcher(paragraph);
        while (matcher.find()) {
            sentences.add(matcher.group());
        }
        return sentences;
    }
}
