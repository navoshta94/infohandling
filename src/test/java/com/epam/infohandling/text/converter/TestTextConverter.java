package com.epam.infohandling.text.converter;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;
import com.epam.infohandling.expression.calculator.Calculator;
import com.epam.infohandling.expression.converter.Converter;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by e.navosha on 23.11.18.
 */
public class TestTextConverter {
    private static final String FIRST_WORD = "First";
    private static final String FIRST_EXPRESSION = "32*2/4+8";
    private static final String SECOND_WORD = "Second";
    private static final String SECOND_EXPRESSION = "3+2/2*4-1";
    private static final String THIRD_WORD = "Third";
    private static final String THIRD_EXPRESSION = "3+5";

    private static final String FIRST_CONVERTED_EXPRESSION = "24.0";
    private static final String SECOND_CONVERTED_EXPRESSION = "6.0";
    private static final String THIRD_CONVERTED_EXPRESSION = "8.0";

    private Component text;

    @Test
    public void shouldReplaceExpressionsByIstValueInText() {
        //given
        initializeTestText();
        Answer calculatorAnswer = createCalculatorAnswer();
        Answer postfixNotationAnswer = createPostfixNotationAnswer();

        Calculator calculator = Mockito.mock(Calculator.class);
        Converter postfixNotation = Mockito.mock(Converter.class);

        Mockito.when(calculator.calculate(anyString())).thenAnswer(calculatorAnswer);
        Mockito.when(postfixNotation.convert(anyString())).thenAnswer(postfixNotationAnswer);

        TextConverter textConverter = new TextConverter(calculator, postfixNotation);
        //when
        Component convertedText = textConverter.convert(text);
        //then
        List<Component> actualParagraphs = convertedText.getChildren();

        Component actualFirstParagraph = actualParagraphs.get(0);
        List<Component> actualFirstParagraphSentences = actualFirstParagraph.getChildren();
        Component firstSentence = actualFirstParagraphSentences.get(0);
        Component secondSentence = actualFirstParagraphSentences.get(1);
        List<Component> firstSentenceElements = firstSentence.getChildren();
        List<Component> secondSentenceElements = secondSentence.getChildren();

        Assert.assertEquals(Lexeme.word(FIRST_WORD), firstSentenceElements.get(0));
        Assert.assertEquals(Lexeme.expression(FIRST_CONVERTED_EXPRESSION), firstSentenceElements.get(1));

        Assert.assertEquals(Lexeme.word(SECOND_WORD), secondSentenceElements.get(0));
        Assert.assertEquals(Lexeme.expression(SECOND_CONVERTED_EXPRESSION), secondSentenceElements.get(1));

        Component actualSecondParagraph = actualParagraphs.get(1);
        List<Component> actualSecondParagraphSentences = actualSecondParagraph.getChildren();
        Component thirdSentence = actualSecondParagraphSentences.get(0);
        List<Component> thirdSentenceElements = thirdSentence.getChildren();

        Assert.assertEquals(Lexeme.word(THIRD_WORD), thirdSentenceElements.get(0));
        Assert.assertEquals(Lexeme.expression(THIRD_CONVERTED_EXPRESSION), thirdSentenceElements.get(1));
    }

    private Answer createCalculatorAnswer() {
        return new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                String expression = (String)invocationOnMock.getArguments()[0];
                switch (expression) {
                    case FIRST_EXPRESSION:
                        return Double.parseDouble(FIRST_CONVERTED_EXPRESSION);
                    case SECOND_EXPRESSION:
                        return Double.parseDouble(SECOND_CONVERTED_EXPRESSION);
                    case THIRD_EXPRESSION:
                        return Double.parseDouble(THIRD_CONVERTED_EXPRESSION);
                    default:
                        throw new IllegalStateException();
                }
            }
        };
    }

    private Answer createPostfixNotationAnswer() {
        return new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return (String)invocationOnMock.getArguments()[0];
            }
        };
    }

    private void initializeTestText() {
        text = new Composite();

        Component firstParagraph = new Composite();
        text.addComponent(firstParagraph);
        Component firstSentenceFirstParagraph = new Composite();
        firstParagraph.addComponent(firstSentenceFirstParagraph);
        firstSentenceFirstParagraph.addComponent(Lexeme.word(FIRST_WORD));
        firstSentenceFirstParagraph.addComponent(Lexeme.expression(FIRST_EXPRESSION));

        Component secondSentenceFirstParagraph = new Composite();
        firstParagraph.addComponent(secondSentenceFirstParagraph);
        secondSentenceFirstParagraph.addComponent(Lexeme.word(SECOND_WORD));
        secondSentenceFirstParagraph.addComponent(Lexeme.expression(SECOND_EXPRESSION));

        Component secondParagraph = new Composite();
        text.addComponent(secondParagraph);
        Component firstSentenceSecondParagraph = new Composite();
        secondParagraph.addComponent(firstSentenceSecondParagraph);
        firstSentenceSecondParagraph.addComponent(Lexeme.word(THIRD_WORD));
        firstSentenceSecondParagraph.addComponent(Lexeme.expression(THIRD_EXPRESSION));
    }
}
