package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by e.navosha on 19.11.18.
 */
public class TestParagraphParser {
    private static final String INPUT_STRING = "Who's there? Hello. Muahaha! Grrr...";
    private static final String FIRST_SENTENCE = "Who's there?";
    private static final String SECOND_SENTENCE = "Hello.";
    private static final String THIRD_SENTENCE = "Muahaha!";
    private static final String FOURTH_SENTENCE = "Grrr...";

    private StringBuilder stringBuilder = new StringBuilder();
    private List<Component> answerComponents;

    @Test
    public void shouldParseParagraphToSentece() {
        //given
        createAnswerComponents();
        Parser parser = Mockito.mock(Parser.class);
        Mockito.when(parser.parse(anyString())).thenAnswer(createAnswer());
        ParagraphParser paragraphParser = new ParagraphParser(parser);
        //when
        Component sentences = paragraphParser.parse(INPUT_STRING);
        //then
        List<Component> actualSentences = sentences.getChildren();
        Assert.assertEquals(4, actualSentences.size());
        Assert.assertEquals(answerComponents.get(0), actualSentences.get(0));
        Assert.assertEquals(answerComponents.get(1), actualSentences.get(1));
        Assert.assertEquals(answerComponents.get(2), actualSentences.get(2));
        Assert.assertEquals(answerComponents.get(3), actualSentences.get(3));
    }

    public Answer createAnswer() {
        return new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                String inputString = (String)invocationOnMock.getArguments()[0];
                switch (inputString) {
                    case FIRST_SENTENCE:
                        return answerComponents.get(0);
                    case SECOND_SENTENCE:
                        return answerComponents.get(1);
                    case THIRD_SENTENCE:
                        return answerComponents.get(2);
                    case FOURTH_SENTENCE:
                        return answerComponents.get(3);
                    default:
                        throw new IllegalStateException();
                }
            }
        };
    }

    private void createAnswerComponents() {
        Component firstSentence = new Composite();
        firstSentence.addComponent(Lexeme.word("Who's"));
        firstSentence.addComponent(Lexeme.word("there?"));

        Component secondSentence = new Composite();
        secondSentence.addComponent(Lexeme.word("Hello."));

        Component thirdSentence = new Composite();
        thirdSentence.addComponent(Lexeme.word("Muahaha!"));

        Component fourthSentence = new Composite();
        fourthSentence.addComponent(Lexeme.word("Grrr..."));

        answerComponents = Arrays.asList(firstSentence,
                                         secondSentence,
                                         thirdSentence,
                                         fourthSentence);
    }

}
