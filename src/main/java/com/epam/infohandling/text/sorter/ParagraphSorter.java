package com.epam.infohandling.text.sorter;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;

import java.util.List;

/**
 * Created by e.navosha on 26.11.18.
 */
public class ParagraphSorter {

    public Component sort(Component text) {
        List<Component> paragraphs = text.getChildren();
        paragraphs.sort((a, b)-> {
            List<Component> sentencesA = a.getChildren();
            List<Component> sentencesB = b.getChildren();
            return sentencesA.size() - sentencesB.size();
        });
        Component sortedText = new Composite();
        paragraphs.forEach(sortedText::addComponent);
        return sortedText;
    }
}
