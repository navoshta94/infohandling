package com.epam.infohandling.expression.terminal;

import com.epam.infohandling.expression.AbstractMathExpression;
import com.epam.infohandling.expression.Context;

public class TerminalExpressionMinus extends AbstractMathExpression {

    @Override
    public void interpret(Context context) {
        double secondValue = context.popValue();
        double firstValue = context.popValue();
        context.pushValue(firstValue - secondValue);
    }
}
