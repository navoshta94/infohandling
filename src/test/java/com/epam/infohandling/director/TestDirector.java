package com.epam.infohandling.director;

import com.epam.infohandling.parser.chain.ChainBuilder;
import com.epam.infohandling.entity.Component;
import com.epam.infohandling.parser.Parser;
import org.junit.Test;

/**
 * Created by e.navosha on 20.11.18.
 */
public class TestDirector {
    private static final String TEXT = "It has survived - not only (five) centuries, but also " +
            "the leap into 50+2 electronic typesetting, remaining 3>>5 essentially 6&9|(3&4) " +
            "unchanged... It was popularised in the 5|(1&2&(3|(4&(2^5|6&47)|3|2|1) with the release " +
            "of Letraset sheets containing Lorem Ipsum passanes, and more recently with desktop " +
            "publishing software like Aldus PageMaker including versions of Lorem Ipsum." +
            "\n    It is a long established fact that a reader will be distracted by the readable " +
            "content of a page when looking at its layout. The point of using 71&(2&3|(3|(2&1>>2|2)&2)|10&2))|78 " +
            "Ipsum is that it has a more-or-less normal distribution of letters, asa opposed to usinf (Content here), " +
            "content here, making it look like readable English." +
            "\n\tIt is a (8^5|1&2<<(2|5>>2&71))|1200 established fact that a reader will be of a page when " +
            "looking at its layout." +
            "\n    Bye.";

    @Test
    public void shouldReturnCorrectResult() {
        ChainBuilder chainBuilder = new ChainBuilder();
        Parser parser = chainBuilder.buildChain();
        Director director = new Director(parser);
        Component component = director.process(TEXT);
        System.out.println(component);
    }
}
