package com.epam.infohandling.reader;

import com.epam.infohandling.exception.DataReaderException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by e.navosha on 26.11.18.
 */
public class DataReaderTest {

    private final static String FILE_PATH =
            "src/test/resources/DataReaderTestFile.txt";
    private final static String FIRST_STRING = "Hello";
    private final static String SECOND_STRING = "java";
    private final static String THIRD_STRING = "world";
    @Test
    public void shouldReturnListOfGivenStringsWhenReadFile()
            throws DataReaderException {
        //given
        DataReader dataReader = new DataReader();
        //when
        List listOfReadedStrings = dataReader.readLines(FILE_PATH);
        //then
        Assert.assertEquals(FIRST_STRING, listOfReadedStrings.get(0));
        Assert.assertEquals(SECOND_STRING, listOfReadedStrings.get(1));
        Assert.assertEquals(THIRD_STRING, listOfReadedStrings.get(2));
    }

    @Test (expected = DataReaderException.class)
    public void shouldReturnDataReaderExceptionWhenFilePathIsWrong()
            throws DataReaderException {
        //given
        DataReader dataReader = new DataReader();
        //when
        dataReader.readLines("");
        //then

    }
}
