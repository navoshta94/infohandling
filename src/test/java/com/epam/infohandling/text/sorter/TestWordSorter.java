package com.epam.infohandling.text.sorter;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;
import com.epam.infohandling.entity.Value;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by e.navosha on 26.11.18.
 */
public class TestWordSorter {
    private static final String FIRST_WORD_FIRST_SENTENCE = "Who";
    private static final String SECOND_WORD_FIRST_SENTENCE = "discovered";
    private static final String THIRD_WORD_FIRST_SENTENCE = "America";

    private static final String FIRST_WORD_SECOND_SENTENCE = "Guess";
    private static final String SECOND_WORD_SECOND_SENTENCE = "who's";
    private static final String THIRD_WORD_SECOND_SENTENCE = "back";

    private Component testText = new Composite();

    @Test
    public void shouldSortSentencesByWordsLength() {
        initTestText();
        WordSorter sorter = new WordSorter();

        Component sortedText = sorter.sort(testText);

        List<Component> paragraphs = sortedText.getChildren();
        Component paragraph = paragraphs.get(0);
        List<Component> sentences = paragraph.getChildren();
        Component firstSentence = sentences.get(0);
        Component secondSentence = sentences.get(1);
        List<Component> wordsFirstSentence = firstSentence.getChildren();
        Assert.assertEquals(3, wordsFirstSentence.size());

        Value actualFirstWordFirstSentence = (Value) wordsFirstSentence.get(0);
        Value actualSecondWordFirstSentence = (Value) wordsFirstSentence.get(1);
        Value actualThirdWordFirstSentence = (Value) wordsFirstSentence.get(2);
        Assert.assertEquals(FIRST_WORD_FIRST_SENTENCE, actualFirstWordFirstSentence.getValue());
        Assert.assertEquals(THIRD_WORD_FIRST_SENTENCE, actualSecondWordFirstSentence.getValue());
        Assert.assertEquals(SECOND_WORD_FIRST_SENTENCE, actualThirdWordFirstSentence.getValue());

        List<Component> wordsSecondSentence = secondSentence.getChildren();
        Assert.assertEquals(3, wordsSecondSentence.size());

        Value actualFirstWordSecondSentence = (Value) wordsSecondSentence.get(0);
        Value actualSecondWordSecondSentence = (Value) wordsSecondSentence.get(1);
        Value actualThirdWordSecondSentence = (Value) wordsSecondSentence.get(2);
        Assert.assertEquals(THIRD_WORD_SECOND_SENTENCE, actualFirstWordSecondSentence.getValue());
        Assert.assertEquals(FIRST_WORD_SECOND_SENTENCE, actualSecondWordSecondSentence.getValue());
        Assert.assertEquals(SECOND_WORD_SECOND_SENTENCE, actualThirdWordSecondSentence.getValue());
    }

    private void initTestText() {
        Component paragraph = new Composite();
        testText.addComponent(paragraph);
        Component firstSentence = new Composite();
        paragraph.addComponent(firstSentence);
        firstSentence.addComponent(Lexeme.word(FIRST_WORD_FIRST_SENTENCE));
        firstSentence.addComponent(Lexeme.word(SECOND_WORD_FIRST_SENTENCE));
        firstSentence.addComponent(Lexeme.word(THIRD_WORD_FIRST_SENTENCE));
        Component secondSentence = new Composite();
        paragraph.addComponent(secondSentence);
        secondSentence.addComponent(Lexeme.word(FIRST_WORD_SECOND_SENTENCE));
        secondSentence.addComponent(Lexeme.word(SECOND_WORD_SECOND_SENTENCE));
        secondSentence.addComponent(Lexeme.word(THIRD_WORD_SECOND_SENTENCE));

    }
}
