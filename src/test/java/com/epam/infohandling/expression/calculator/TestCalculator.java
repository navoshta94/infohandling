package com.epam.infohandling.expression.calculator;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by e.navosha on 22.11.18.
 */
public class TestCalculator {
    private final static String EXPRESSION = "2 8 3 * 4 / + 1 -";
    private final static Double EXPECTED_RESULT = 7.0;
    private final static double DELTA = 0.01;

    @Test
    public void ShouldReturnCorrectResultOfCalculation() {
        //given
        Calculator calculator = new Calculator();
        //when
        Double actualResult = calculator.calculate(EXPRESSION);
        //then
        Assert.assertEquals(EXPECTED_RESULT, actualResult, DELTA);
    }
}
