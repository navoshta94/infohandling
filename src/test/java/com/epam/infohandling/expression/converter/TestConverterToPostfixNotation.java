package com.epam.infohandling.expression.converter;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by e.navosha on 22.11.18.
 */
public class TestConverterToPostfixNotation {
    private static final String TEST_STRING = "2+8*3/4-1";

    private static final String EXPECTED_STRING = "2 8 3 * 4 / + 1 -";
    @Test
    public void shouldReturnPostfixNotationOfGivenString() {
        ConverterToPostfixNotation converter = new ConverterToPostfixNotation();

        String actualString = converter.convert(TEST_STRING);

        Assert.assertEquals(EXPECTED_STRING, actualString);
    }
}
