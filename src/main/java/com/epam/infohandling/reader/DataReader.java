package com.epam.infohandling.reader;

import com.epam.infohandling.exception.DataReaderException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by e.navosha on 26.11.18.
 */
public class DataReader {
    public List readLines(String path) throws DataReaderException {
        List<String> listLines = new ArrayList<String>();
        Scanner scanner = null;
        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            scanner = new Scanner(bufferedReader);
            while(scanner.hasNext()) {
                String line = scanner.nextLine();
                listLines.add(line);
            }
        } catch (IOException e) {
            throw new DataReaderException(e.getMessage(), e);
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return listLines;
    }
}
