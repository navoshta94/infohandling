package com.epam.infohandling.expression.converter;

/**
 * Created by e.navosha on 22.11.18.
 */
public enum Operation {
    PLUS("+", 0), MINUS("-", 0), DIVIDE("/", 1), MULTIPLY("*", 1);

    private final String sign;
    private final int priority;

    private Operation(String sign, int priority) {
        this.sign = sign;
        this.priority = priority;
    }

    public String getSign() {
        return sign;
    }

    public int getPriority() {
        return priority;
    }
}
