package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser extends Parser {
    private static final String SPLIT_REGEX = " ";
    private static final String EXPRESSION_REGEX = "(\\d+|(\\+|\\/|-|\\*)*)+";
    private static final Pattern EXPRESSION_PATTERN = Pattern.compile(EXPRESSION_REGEX);

    public SentenceParser() {

    }

    public Component parse(String sentence) {
        String[] lexems = sentence.split(SPLIT_REGEX);
        return acquireComponent(lexems);
    }

    private Component acquireComponent(String[] lexemes) {
        Component component = new Composite();
        Arrays.stream(lexemes).forEach(o -> component.addComponent(createLexeme(o)));
        return component;
    }

    private Component createLexeme(String lexeme) {
        Matcher matcher = EXPRESSION_PATTERN.matcher(lexeme);
        if (matcher.matches()) {
            return Lexeme.expression(lexeme);
        } else{
            return Lexeme.word(lexeme);
        }
    }

}
