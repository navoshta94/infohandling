package com.epam.infohandling.text.sorter;

import com.epam.infohandling.TestObjects;
import com.epam.infohandling.entity.Component;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by e.navosha on 26.11.18.
 */
public class TestParagraphSorter {
    private static final int INDEX_FIRST_PARAGRAPH = 1;
    private static final int INDEX_SECOND_PARAGRAPH = 0;
    private static final int INDEX_THIRD_PARAGRAPH = 2;
    private Component expectedFirstParagraph;
    private Component expectedSecondParagraph;
    private Component expectedThirdParagraph;

    @Test
    public void shouldSortParagraphsByItsSentenceAmount() {
        ParagraphSorter sorter = new ParagraphSorter();
        initExpectedSortedText();

        Component sortedText = sorter.sort(TestObjects.testText);

        List<Component> sortedParagraphs = sortedText.getChildren();
        Assert.assertEquals(3, sortedParagraphs.size());
        Assert.assertEquals(expectedFirstParagraph, sortedParagraphs.get(0));
        Assert.assertEquals(expectedSecondParagraph, sortedParagraphs.get(1));
        Assert.assertEquals(expectedThirdParagraph, sortedParagraphs.get(2));
    }

    private void initExpectedSortedText() {
        List<Component> paragraphs = TestObjects.testText.getChildren();
        expectedFirstParagraph = paragraphs.get(INDEX_FIRST_PARAGRAPH);
        expectedSecondParagraph = paragraphs.get(INDEX_SECOND_PARAGRAPH);
        expectedThirdParagraph = paragraphs.get(INDEX_THIRD_PARAGRAPH);
    }
}
