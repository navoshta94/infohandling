package com.epam.infohandling.text.sorter;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;
import com.epam.infohandling.entity.Value;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.navosha on 26.11.18.
 */
public class WordSorter {

    public Component sort(Component text) {
        List<Component> paragraphsForSort = text.getChildren();
        Component sortedText = new Composite();
        for (Component paragraph : paragraphsForSort) {
            List<Component> sentences = paragraph.getChildren();
            Component sortedParagraph = new Composite();
            sortedText.addComponent(sortedParagraph);
            for (Component sentence : sentences) {
                sortedParagraph.addComponent(sortWordsByLength(sentence));
            }
        }

        return sortedText;
    }

    private Component sortWordsByLength(Component sentence) {
        Component sortedSentence = new Composite();
        List<Component> lexemes = sentence.getChildren();
        List<Component> words = aquireWords(lexemes);
        words.sort((a, b) -> {
            Value firstWord = (Value) a;
            Value secondWord = (Value) b;
            String firstValue = firstWord.getValue();
            String secondValue = secondWord.getValue();
            return firstValue.length() - secondValue.length();
        });
        words.forEach(sortedSentence::addComponent);
        return sortedSentence;
    }

    private List<Component> aquireWords(List<Component> lexemes) {
        List<Component> words = new ArrayList<>();
        for (Component lexeme : lexemes) {
            Lexeme forCheck = (Lexeme) lexeme;
            if (!forCheck.isExpression()) {
                words.add(forCheck);
            }
        }
        return words;
    }
}
