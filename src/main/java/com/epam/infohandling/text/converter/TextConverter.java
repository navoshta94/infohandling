package com.epam.infohandling.text.converter;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;
import com.epam.infohandling.entity.Value;
import com.epam.infohandling.expression.calculator.Calculator;
import com.epam.infohandling.expression.converter.Converter;

import java.util.List;

/**
 * Created by e.navosha on 23.11.18.
 */
public class TextConverter {
    private Calculator calculator;
    private Converter converter;

    public TextConverter(Calculator calculator, Converter converter) {
        this.calculator = calculator;
        this.converter = converter;
    }

    public Component convert(Component inputComponent) {
        Component newComponent = new Composite();
        if (inputComponent.getChildren().size() != 0) {
            List<Component> children = inputComponent.getChildren();
            children.forEach(o -> {
                newComponent.addComponent(convert(o));
            });
        } else {
            Lexeme lexeme = (Lexeme) inputComponent;
            String value = lexeme.getValue();
            if (lexeme.isExpression()) {
                String convertedValue = converter.convert(value);
                Double calculatedValue = calculator.calculate(convertedValue);
                return Lexeme.expression(String.valueOf(calculatedValue));
            } else {
                return Lexeme.word(value);
            }
        }
        return newComponent;
    }
}
