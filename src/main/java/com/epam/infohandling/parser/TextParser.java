package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;

import java.util.Arrays;

public class TextParser extends Parser {
    private static final String SPLIT_REGEX = "\\n(\\t|\\s{4})";
    private Parser successor;

    public TextParser(){}
    public TextParser(Parser successor) {
        this.successor = successor;
    }

    @Override
    public Component parse(String text) {
        String[] paragraphs = text.split(SPLIT_REGEX);
        Component component = new Composite();
        Arrays.stream(paragraphs).forEach(o -> component
                .addComponent(successor.parse(o)));
        return component;
    }
}
