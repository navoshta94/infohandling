package com.epam.infohandling.expression.calculator;

import com.epam.infohandling.expression.AbstractMathExpression;
import com.epam.infohandling.expression.Context;
import com.epam.infohandling.expression.nonterminal.NonterminalExpression;
import com.epam.infohandling.expression.terminal.TerminalExpressionDivide;
import com.epam.infohandling.expression.terminal.TerminalExpressionMinus;
import com.epam.infohandling.expression.terminal.TerminalExpressionMultiply;
import com.epam.infohandling.expression.terminal.TerminalExpressionPlus;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by e.navosha on 21.11.18.
 */
public class Calculator {
    private final static String GROUP_REGEX = "\\d+|\\*|/|\\+|-";
    private final static Pattern PATTERN = Pattern.compile(GROUP_REGEX);
    private List<AbstractMathExpression> listExpression;

    public Calculator() {
        listExpression = new ArrayList<>();
    }

    private void parseExperession(String expression) {
        Matcher matcher = PATTERN.matcher(expression);
        while (matcher.find()) {
            addExpression(matcher.group());
        }
    }

    private void addExpression(String expressionCharacter) {
        switch (expressionCharacter) {
            case "+":
                listExpression.add(new TerminalExpressionPlus());
                break;
            case "-":
                listExpression.add(new TerminalExpressionMinus());
                break;
            case "*":
                listExpression.add(new TerminalExpressionMultiply());
                break;
            case "/":
                listExpression.add(new TerminalExpressionDivide());
                break;
            default:
                double value = Integer.parseInt(expressionCharacter);
                listExpression.add(new NonterminalExpression(value));
        }
    }

    public Double calculate(String expression) {
        listExpression = new ArrayList<>();
        parseExperession(expression);
        Context context = new Context();
        for (AbstractMathExpression terminal : listExpression) {
            terminal.interpret(context);
        }
        return context.popValue();
    }

}
