package com.epam.infohandling.expression;

public abstract class AbstractMathExpression {

    public abstract void interpret(Context context);
}
