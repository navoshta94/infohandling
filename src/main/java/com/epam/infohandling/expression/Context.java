package com.epam.infohandling.expression;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by e.navosha on 21.11.18.
 */
public class Context {
    private Deque<Double> contextValues = new ArrayDeque<>();

    public Double popValue() {
        return contextValues.pop();
    }

    public void pushValue(Double value) {
        contextValues.push(value);
    }
}
