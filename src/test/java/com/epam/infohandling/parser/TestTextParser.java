package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Composite;
import com.epam.infohandling.entity.Lexeme;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by e.navosha on 19.11.18.
 */
public class TestTextParser {

    private static final String TEXT = "Paragraph1!\n\tParagraph2.\n    Paragraph3...";
    private static final String FIRST_PARAGRAPH = "Paragraph1!";
    private static final String SECOND_PARAGRAPH = "Paragraph2.";
    private static final String THIRD_PARAGRAPH = "Paragraph3...";

    private List<Component> answerComponents;

    @Test
    public void shouldParserTextToParagraphs() {
        //given
        createAnswerComponents();
        Parser paragraphParser = Mockito.mock(Parser.class);
        Mockito.when(paragraphParser.parse(anyString())).thenAnswer(createAnswer());
        TextParser textParser = new TextParser(paragraphParser);
        //when
        Component text = textParser.parse(TEXT);
        List<Component> actualComponents = text.getChildren();
        Assert.assertEquals(3, actualComponents.size());
        Assert.assertEquals(answerComponents.get(0), actualComponents.get(0));
        Assert.assertEquals(answerComponents.get(1), actualComponents.get(1));
        Assert.assertEquals(answerComponents.get(2), actualComponents.get(2));
    }

    private void createAnswerComponents() {
        Component firstParagraph = new Composite();
        Component sentenceFirstParagraph = new Composite();
        firstParagraph.addComponent(sentenceFirstParagraph);
        sentenceFirstParagraph.addComponent(Lexeme.word(FIRST_PARAGRAPH));


        Component secondParagraph = new Composite();
        Component sentenceSecondParagraph = new Composite();
        secondParagraph.addComponent(sentenceSecondParagraph);
        sentenceSecondParagraph.addComponent(Lexeme.word(SECOND_PARAGRAPH));

        Component thirdParagraph = new Composite();
        Component sentenceThirdParagraph = new Composite();
        thirdParagraph.addComponent(sentenceSecondParagraph);
        sentenceThirdParagraph.addComponent(Lexeme.word(THIRD_PARAGRAPH));

        answerComponents = Arrays.asList(firstParagraph,
                                         secondParagraph,
                                         thirdParagraph);
    }

    public Answer createAnswer () {

        return new Answer<Component>() {
            @Override
            public Component answer(InvocationOnMock invocationOnMock) throws Throwable {
                String paragraph = (String) invocationOnMock.getArguments()[0];
                switch (paragraph) {
                    case FIRST_PARAGRAPH:
                        return answerComponents.get(0);
                    case SECOND_PARAGRAPH:
                        return answerComponents.get(1);
                    case THIRD_PARAGRAPH:
                        return answerComponents.get(2);
                    default:
                        throw new IllegalStateException();
                }
            }
        };
    }
}
