package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;

public abstract class Parser {
    private Parser successor;

    public Parser(){

    }

    public Parser(Parser successor) {
        this.successor = successor;
    }

    public abstract Component parse(String parse);

    public Parser getSuccessor() {
        return successor;
    }

    public Parser setSuccessor(Parser successor) {
        this.successor = successor;
        return successor;
    }
}
