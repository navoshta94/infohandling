package com.epam.infohandling.expression.converter;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by e.navosha on 22.11.18.
 */
public class ConverterToPostfixNotation implements Converter{
    private final static String GROUP_REGEX = "\\d+|\\*|/|\\+|-";
    private final static Pattern PATTERN = Pattern.compile(GROUP_REGEX);
    private final static String ELEMENTS_SEPARATOR = " ";
    private List<String> expressionElements = new ArrayList<>();

    private Deque<Operation> operationsList = new ArrayDeque<>();
    private StringBuilder postfixNotation = new StringBuilder();

    public String convert(String expression) {
        parseExperession(expression);
        for (String element : expressionElements) {
            switch (element) {
                case "+":
                    addOperation(Operation.PLUS);
                    break;
                case "-":
                    addOperation(Operation.MINUS);
                    break;
                case "*":
                    addOperation(Operation.MULTIPLY);
                    break;
                case "/":
                    addOperation(Operation.DIVIDE);
                    break;
                default:
                    postfixNotation.append(element+ ELEMENTS_SEPARATOR);
            }
        }
        if (operationsList.size() > 0) {
            Operation lastOperation = operationsList.pop();
            postfixNotation.append(lastOperation.getSign());
        }
        return postfixNotation.toString();
    }

    private void addOperation(Operation currentOperation) {
        Operation prevOperation = operationsList.peekLast();
        while (operationsList.size() > 0 && currentOperation.getPriority() <= prevOperation.getPriority()) {
            operationsList.removeLast();
            postfixNotation.append(prevOperation.getSign() + ELEMENTS_SEPARATOR);
            prevOperation = operationsList.peekLast();
        }
        operationsList.add(currentOperation);
    }

    private void parseExperession(String expression) {
        Matcher matcher = PATTERN.matcher(expression);
        while (matcher.find()) {
            expressionElements.add(matcher.group());
        }
    }
}
