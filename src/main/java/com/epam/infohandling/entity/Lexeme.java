package com.epam.infohandling.entity;

import com.epam.infohandling.expression.calculator.Calculator;

import java.util.ArrayList;
import java.util.List;

public class Lexeme implements Component, Value{
    private final String value;
    private boolean expression;

    private Lexeme(String value, boolean expression){
        this.value = value;
        this.expression = expression;
    }

    public static Lexeme word(String lexeme) {
        return new Lexeme(lexeme, false);
    }

    public static Lexeme expression(String lexeme) {
        return new Lexeme(lexeme, true);
    }


    public void addComponent(Component component) {
        throw new UnsupportedOperationException();
    }

    public List<Component> getChildren() {
        return new ArrayList<>();
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean isExpression() {
        return expression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lexeme lexeme = (Lexeme) o;

        if (expression != lexeme.expression) return false;
        return value != null ? value.equals(lexeme.value) : lexeme.value == null;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (expression ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Lexeme{" +
                "value='" + value + '\'' +
                ", expression=" + expression +
                '}';
    }
}
