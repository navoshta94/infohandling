package com.epam.infohandling.expression.terminal;

import com.epam.infohandling.expression.AbstractMathExpression;
import com.epam.infohandling.expression.Context;

public class TerminalExpressionMultiply extends AbstractMathExpression {

    @Override
    public void interpret(Context context) {
        context.pushValue(context.popValue() * context.popValue());
    }
}
