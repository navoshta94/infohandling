package com.epam.infohandling.exception;

/**
 * Created by e.navosha on 26.11.18.
 */
public class DataReaderException extends Exception{

    public DataReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
