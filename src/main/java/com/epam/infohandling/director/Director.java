package com.epam.infohandling.director;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.parser.Parser;

/**
 * Created by e.navosha on 20.11.18.
 */
public class Director {
    private Parser parser;

    public Director(Parser parser) {
        this.parser = parser;
    }

    public Component process(String text) {
        return parser.parse(text);
    }
}
