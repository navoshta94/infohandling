package com.epam.infohandling.parser.chain;

import com.epam.infohandling.parser.ParagraphParser;
import com.epam.infohandling.parser.Parser;
import com.epam.infohandling.parser.SentenceParser;
import com.epam.infohandling.parser.TextParser;

public class ChainBuilder {

    public Parser buildChain() {
        return new TextParser()
                .setSuccessor(new ParagraphParser())
                .setSuccessor(new SentenceParser());


    }
}
