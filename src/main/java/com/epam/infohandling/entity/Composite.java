package com.epam.infohandling.entity;

import java.util.ArrayList;
import java.util.List;

public class Composite implements Component{
    private List<Component> components = new ArrayList<Component>();

    public void addComponent(Component component) {
        components.add(component);
    }

    public List<Component> getChildren() {
        return components;
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        sb.append(components.stream().toString());
        return sb.toString();
    }

    @Override
    public String toString() {
        /*return "Composite{" +
                "components=" + components +
                '}';*/
        StringBuilder sb = new StringBuilder();
        components.stream().forEach(sb::append);
        return sb.toString();
    }
}
