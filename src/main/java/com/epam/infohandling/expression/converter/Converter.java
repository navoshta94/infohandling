package com.epam.infohandling.expression.converter;

/**
 * Created by e.navosha on 22.11.18.
 */
public interface Converter {
    String convert(String expression);
}
