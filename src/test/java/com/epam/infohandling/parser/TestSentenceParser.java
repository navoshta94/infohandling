package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.Lexeme;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TestSentenceParser {
    private static final String SENTENCE = "This is simple sentence, 3+5";
    private static final Component FIRST_COMPONENT = Lexeme.word("This");
    private static final Component SECOND_COMPONENT = Lexeme.word("is");
    private static final Component THIRD_COMPONENT = Lexeme.word("simple");
    private static final Component FOURH_COMPONENT = Lexeme.word("sentence,");
    private static final Component FIFTH_COMPONENT = Lexeme.expression("3+5");

    @Test
    public void shouldParseSentencesToWord() {
        //given
        Parser parser = new SentenceParser();
        //when
        Component component = parser.parse(SENTENCE);
        //then
        List<Component> words = component.getChildren();
        Assert.assertEquals(5, words.size());
        Assert.assertEquals(FIRST_COMPONENT, words.get(0));
        Assert.assertEquals(SECOND_COMPONENT, words.get(1));
        Assert.assertEquals(THIRD_COMPONENT, words.get(2));
        Assert.assertEquals(FOURH_COMPONENT, words.get(3));
        Assert.assertEquals(FIFTH_COMPONENT, words.get(4));
    }
}
